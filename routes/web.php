<?php  

Route::get('/', 'PagesController@home');
Route::get('/about', 'PagesController@about');

Route::get('/siswa', 'SiswaController@index');

// Students
Route::get('/students', 'studentsController@index');

Route::get('/students/create', 'studentsController@create');
Route::get('/students/{student}', 'studentsController@show');
Route::post('/students', 'studentsController@store');
Route::delete('/students/{student}', 'studentsController@destroy');
Route::get('/students/{student}/edit', 'studentsController@edit');
Route::patch('/students/{student}', 'studentsController@update');