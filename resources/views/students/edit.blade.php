@extends('layout.main')

  @section('title', 'Form Ubah Data Siswa')

@section('container')
<div class="container">
  <div class="row">
    <div class="col-8">
  		<h1 class="mt-3">Form Ubah Data Siswa</h1>

      <form method="post" action="/students/{{ $student->id }}">
        @method('patch')
        @csrf
          <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama"placeholder="masukan Nama" name="nama" value="{{ $student->nama }}">
           <div class="invalid-feedback">
          Please choose a username.
        </div>
          </div>
          <div class="form-group">
          <label for="nik">Nik</label>
          <input type="text" class="form-control" id="nik"placeholder="masukan Nik" name="nik" value="{{ $student->nik }}">
          </div>
          <div class="form-group">
          <label for="email">Email</label>
          <input type="text" class="form-control" id="email"placeholder="masukan Email" name="email" value="{{ $student->email }}">
          </div>
          <div class="form-group">
          <label for="jurusan">Jurusan</label>
          <input type="text" class="form-control" id="jurusan"placeholder="masukan Jurusan" name="jurusan" value="{{ $student->jurusan }}">
          </div>
          <button type="sumbit" class="btn btn-primary">Ubah Data!</button>
      </form>      
    </div>
  </div>
</div>
@endsection