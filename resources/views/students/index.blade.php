@extends('layout.main')

  @section('title', 'Daftar Siswa')

@section('container')
<div class="container">
  <div class="row">
    <div class="col-6">
  		<h1 class="mt-3">Daftar Siswa</h1>

      <a href="/students/create" class="btn btn-primary my-3">Tambah Data Siswa</a>

      @if (session('status'))
      <dir class="alert alert-success">
        {{ session('status') }}
      </dir>
      @endif

      <ul class="list-group">
        @foreach( $students as $student )
        <li class="list-group-item d-flex justify-content-between align-items-center">
          {{ $student->nama }}
          <span class="badge badge-primary badge-pill">14</span>
            <a href="/students/{{ $student->id }}" class="badge badge-info">detail</a>
        </li>
        @endforeach
      </ul>
    </div>
  </div>
</div>
@endsection