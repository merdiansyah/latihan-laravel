@extends('layout.main')

  @section('title', 'Form Tambah Data Siswa')

@section('container')
<div class="container">
  <div class="row">
    <div class="col-8">
  		<h1 class="mt-3">Form Tambah Data Siswa</h1>

      <form method="post" action="/students">
        @csrf
          <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama"placeholder="masukan Nama" name="nama" value="{{ old('nama') }}">
           <div class="invalid-feedback">
          Please choose a username.
        </div>
          </div>
          <div class="form-group">
          <label for="nik">Nik</label>
          <input type="text" class="form-control" id="nik"placeholder="masukan Nik" name="nik" value="{{ old('nik') }}">
          </div>
          <div class="form-group">
          <label for="email">Email</label>
          <input type="text" class="form-control" id="email"placeholder="masukan Email" name="email" value="{{ old('email') }}">
          </div>
          <div class="form-group">
          <label for="jurusan">Jurusan</label>
          <input type="text" class="form-control" id="jurusan"placeholder="masukan Jurusan" name="jurusan" value="{{ old('jurusan') }}">
          </div>
          <button type="sumbit" class="btn btn-primary">Tambah Data!</button>
      </form>      
    </div>
  </div>
</div>
@endsection